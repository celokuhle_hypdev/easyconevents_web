## How to use the app
* You can access this app [here](https://easy-confevents.herokuapp.com/).

## Test and run this app on local machine
* Run 'npm install' to install all dependencies listed on the package.json file.
* To modify the backend URL modify the const on proxy.js

## Security on the app
* Users are required to login, and their passwords are stored in encypted form in the database.
* Most of the resources require a JSON web token to access them, ontop of login.


## 3rd Party Libraries
* [Create React App](https://github.com/facebook/create-react-app) was used to bootstrap this project.
* react-router-dom - for routing accross multiple components
* axios - used as a rest http client to communitcate with the backend
* jwt-decode - to decode jwt token from the express server inorder to determine the role of the user, admin or not. 

## App deployment:
* The app is deployed on heroku on: https://easy-confevents.herokuapp.com/
* mars buildpack -  used in Heroku to build this react app