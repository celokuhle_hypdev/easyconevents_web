import React from "react";
import { handleHttpError } from "./errorUtil";
import axios from "axios";
import { getToken } from "./sessionUtil";
import URL from "./proxy";

class EditEvent extends React.Component {
  /**
   * location prop provided by react-router-dom, just like the history prop, and is used to access the history's state
   * 
   * the location object is never mutated so you can use it in the lifecycle hook
   *  to determine when navigation happens, this is really useful for data fetching and animation.
   * 
   * you can pass a location to the following components:  Route, Switch
   * 
   * See more on location and many more: https://reactrouter.com/web/api/location 
   
   */
  constructor(props) {
    super(props);

    let temp = this.props.location.state;
    this.prePopulateDate(temp); //works like pass-by-ref
    this.state = temp;
  }

  /**
   * /event/${this.state._id} - I spent too much time debugging this,
   *  only to find that the 404 status error I was getting was because I was not passing
   *  in the _id parameter like below:
   *
   * /event/${this.state._id}
   *
   */
  onSubmit = (event) => {
    event.preventDefault();

    axios
      .put(`${URL}/event/${this.state._id}`, this.state, {
        headers: { authorization: getToken() },
      })
      .then((res) => {
        alert("Success: Upated.");
        this.props.history.push("/events/list");
      })
      .catch((reason) => {
        handleHttpError(reason.message);
      });
  };

  onStateChange = (event) => {
    let nam = event.target.name;
    let val = event.target.value;

    this.setState({
      [nam]: val,
    });
  };

  onChecked = (event) => {
    this.setState({
      isActive: !this.state.isActive,
    });
  };

  prePopulateDate(confEvent) {
    var date = new Date(confEvent.date);
    confEvent.date = date.toISOString().substr(0, 10);
  }

  render() {
    return (
      <form className="left-align body" onSubmit={this.onSubmit}>
        <h4>Edit Event</h4>
        <br />
        <div className="row">
          <div className="col-md-3">
            <label>Event Name:</label>
          </div>
          <div className="col-md-4">
            <input
              className="full-width"
              name="name"
              defaultValue={this.state.name}
              onChange={this.onStateChange}
            />
          </div>
        </div>
        <br />

        <div className="row">
          <div className="col-md-3">Details:</div>
          <div className="col-md-4">
            <input
              className="full-width"
              name="description"
              defaultValue={this.state.description}
              onChange={this.onStateChange}
            />
          </div>
        </div>
        <br />

        <div className="row">
          <div className="col-md-3">Place Or Location:</div>
          <div className="col-md-4">
            <input
              className="full-width"
              name="location"
              defaultValue={this.state.location}
              onChange={this.onStateChange}
            />
          </div>
        </div>
        <br />

        <div className="row">
          <div className="col-md-3">Event Date:</div>
          <div className="col-md-4">
            <input
              name="date"
              type="date"
              title={this.state.date}
              value={this.state.date}
              onChange={this.onStateChange}
            />
          </div>
        </div>
        <br />

        <div className="row">
          <div className="col-md-3">
            Event Duration <small>e.g. 09:00am - 15:00pm</small>:
          </div>
          <div className="col-md-4">
            <input
              className="full-width"
              name="duration"
              defaultValue={this.state.duration}
              onChange={this.onStateChange}
            />
          </div>
        </div>
        <br />

        <div className="row">
          <div className="col-md-3">Entry Price:</div>
          <div className="col-md-4">
            <input
              name="entryPrice"
              type="number"
              defaultValue={this.state.entryPrice}
              onChange={this.onStateChange}
            />
          </div>
        </div>
        <br />

        <div className="row">
          <div className="col-md-3">Make Available For Booking:</div>
          <div className="col-md-4">
            <input
              type="checkbox"
              name="isActive"
              defaultChecked={this.state.isActive}
              onChange={this.onChecked}
            />
          </div>
        </div>
        <br />

        <div className="row">
          <div className="col-md-2">
            <br />
            <input type="submit" value="Submit" />
          </div>
        </div>
        <br />
      </form>
    );
  }
}

export default EditEvent;
