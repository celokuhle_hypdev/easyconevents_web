import React from "react";
import axios from "axios";
import { handleHttpError } from "./errorUtil";
import { isAuthenticated, getToken } from "./sessionUtil";
import URL from "./proxy";

class CreateEvent extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isActive: true,
    };
  }

  componentDidMount() {
    if (!isAuthenticated()) {
      this.props.history.push("/login");
    }
  }

  /** 
  - event.preventDefault() prevents the form submission event from firing, 
  as all data in already updated in the state by the onChange event of the inputs 
 
  - The base URL used for axios is saved on package.json in the proxy field.
   
  - NB: if the Component is already a child of react-router-dom.Route then the 'history' prop
   is automatically imported into this component. 
   
  */
  onSubmit = (event) => {
    event.preventDefault();

    const payload = this.state;
    axios
      .post(`${URL}/event`, payload, { headers: { authorization: getToken() } })
      .then((res) => {
        alert("Success: Created.");
        this.props.history.push("/events/list");
      })
      .catch((reason) => {
        handleHttpError(reason.message);
      });
  };

  onStateChange = (event) => {
    let nam = event.target.name;
    let val = event.target.value;

    this.setState({
      [nam]: val,
    });
  };

  onChecked = (event) => {
    this.setState({
      isActive: !this.state.isActive,
    });
  };

  render() {
    return (
      <form className="left-align body" onSubmit={this.onSubmit.bind()}>
        <h4>Create Event</h4>
        <br />
        <div className="row">
          <div className="col-md-3">
            <label>Event Name:</label>
          </div>
          <div className="col-md-4">
            <input
              className="full-width"
              name="name"
              onChange={this.onStateChange}
              required
            />
          </div>
        </div>
        <br />

        <div className="row">
          <div className="col-md-3">Details:</div>
          <div className="col-md-4">
            <input
              className="full-width"
              name="description"
              onChange={this.onStateChange}
              required
            />
          </div>
        </div>
        <br />

        <div className="row">
          <div className="col-md-3">Place Or Location:</div>
          <div className="col-md-4">
            <input
              className="full-width"
              name="location"
              onChange={this.onStateChange}
              required
            />
          </div>
        </div>
        <br />

        <div className="row">
          <div className="col-md-3">Event Date:</div>
          <div className="col-md-4">
            <input
              name="date"
              type="date"
              onChange={this.onStateChange}
              required
            />
          </div>
        </div>
        <br />

        <div className="row">
          <div className="col-md-3">
            Event Duration <small>e.g. 09:00am - 15:00pm</small>:
          </div>
          <div className="col-md-4">
            <input
              className="full-width"
              name="duration"
              onChange={this.onStateChange}
              required
            />
          </div>
        </div>
        <br />

        <div className="row">
          <div className="col-md-3">Entry Price:</div>
          <div className="col-md-4">
            <input
              name="entryPrice"
              type="number"
              onChange={this.onStateChange}
              required
            />
          </div>
        </div>
        <br />

        <div className="row">
          <div className="col-md-3">Make Available For Booking:</div>
          <div className="col-md-4">
            <input
              type="checkbox"
              checked={this.state.isActive}
              name="isActive"
              onChange={this.onChecked}
            />
          </div>
        </div>
        <br />

        <div className="row">
          <div className="col-md-2">
            <br />
            <input type="submit" value="Submit" />
          </div>
        </div>
        <br />
      </form>
    );
  }
}

export default CreateEvent;
