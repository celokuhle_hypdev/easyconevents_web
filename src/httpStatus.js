const HttpStatus = {
  OK: 200,
  UNAUTHORISED: 401,
  ACCESS_DENIED: 403,
  SUCCESSFULLY_CREATED: 201,
  INTERNAL_SERVER_ERROR: 500,
  BAD_REQUEST: 400,
};

export default HttpStatus;