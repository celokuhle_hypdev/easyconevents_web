export default class User {
  constructor(name, surname, dateOfBirth, email, password, isAdmin) {
    this.name = name;
    this.surname = surname;
    this.dateOfBirth = dateOfBirth;
    this.email = email;
    this.password = password;
    this.isAdmin = isAdmin; 
  }
}
