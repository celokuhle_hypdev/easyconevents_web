class Event {
  constructor(
    _id,
    name,
    description,
    location,
    date,
    duration,
    entryPrice,
    isActive
  ) {
    this._id = _id;
    this.name = name;
    this.description = description;
    this.location = location;
    this.date = date;
    this.duration = duration;
    this.entryPrice = entryPrice;
    this.isActive = isActive;
  }
}

export default Event;