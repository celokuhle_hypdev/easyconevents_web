import React, { Component } from "react";
import axios from "axios";
import { handleHttpError } from "./errorUtil";
import { setAuth } from "./sessionUtil";
import URL from "./proxy";

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      email: "",
      password: "",
    };
  }

  onSubmit = (event) => {
    event.preventDefault();

    axios
      .post(`${URL}/user/login`, this.state)
      .then((res) => {
        let token = res.headers["authorization"];

        setAuth(token);
        this.props.history.push("/events/list");
      })
      .catch((reason) => {
        handleHttpError(reason.message);
      });
  };

  onStateChange = (event) => {
    let nam = event.target.name;
    let val = event.target.value;

    this.setState({
      [nam]: val,
    });
  };

  render() {
    return (
      <form className="left-align body" onSubmit={this.onSubmit}>
        <h4>Login</h4>
        <br />

        <div className="row">
          <div className="col-md-3">Email:</div>
          <div className="col-md-4">
            <input
              type="email"
              name="email"
              className="full-width"
              onChange={this.onStateChange}
            />
          </div>
        </div>
        <br />

        <div className="row">
          <div className="col-md-3">Password:</div>
          <div className="col-md-4">
            <input
              type="password"
              name="password"
              className="full-width"
              onChange={this.onStateChange}
            />
          </div>
        </div>
        <br />

        <div className="row">
          <div className="col-md-2">
            <br />
            <input type="submit" value="Submit" />
          </div>
        </div>

        <br />

        <div className="row">
          <div className="col-md-3">
            <div>
              <small>New to the site?</small>
              <div
                onClick={() => {
                  this.props.history.push("/register");
                }}
              >
                <u className="link">
                  <small>Register here.</small>
                </u>
              </div>
            </div>
          </div>
        </div>
      </form>
    );
  }
}
export default Login;
