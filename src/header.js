function Header() {
  return (
    <div style={{ backgroundColor: "LightGreen", padding: "1%" }}>
      <div className="row">
        <div className="col-md-4">
          <h3 style={{ textAlign: "left" }}>EasyConEvents</h3>
        </div>
        <div className="col-md-4"></div>
        <div className="col-md-4">
          <span style={{ textAlign: "left" }}>
            Booking and management of conference events
          </span>
        </div>
      </div>
    </div>
  );
}

export default Header;
