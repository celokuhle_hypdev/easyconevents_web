import { React, useEffect, useState } from "react";
import { handleHttpError } from "./errorUtil";
import axios from "axios";
import { useHistory } from "react-router-dom";
import { isAuthenticated, getToken, removeAuth, isAdmin } from "./sessionUtil";
import URL from "./proxy";


function ListEvent() {
  const [confEvents, setConfEvents] = useState([]);

  let history = useHistory();

  /**
   UseEffects:
   - When the second parameter is an empty list, the callback will only be fired once, similar to componentDidMount. 
   - When the second parameter has prop, the callback will fired everytime that component is updated , similar to componentDidUpdate. 
   
   - return function cleanup():  similar to componenWillUnmount
   */
  useEffect(() => {
    if (!isAuthenticated()) {
      history.push("/login");
      return;
    }
    loadData();

    /**
     * Error received before the cleanup part was added:
     *    "Cant perform a React state update on an unmounted component.
     *    This is a no-op, but it indicates a memory leak in your application.""
     */
    return () => {};
  }, [confEvents]); //eslint-disable-line react-hooks/exhaustive-deps

  /**
   * Introduced a seperate function for making asychronous calls because useEffect is synchronous
   */
  async function loadData() {
    await axios
      .get(`${URL}/event`, { headers: { authorization: getToken() } })
      .then((res) => {
        setConfEvents(res.data);
      })
      .catch((reason) => {
        handleHttpError(reason.message);
        removeAuth();
        history.push("/login");
      });
  }

  /**
   * Error thrown when this method's name started with lowercase:
   * {
   * React Hook "useHistory" is called in function "handleDelete" that
   * is neither a React function component nor a custom React Hook function.
   * React component names must start with an uppercase letter
   * }
   *
   */
  function HandleUpdate(confEvent) {
    delete confEvent.__v; //remove db version
    history.push("/events/edit/", confEvent);
  }

  /**
   * This method was being called on pageLoad, until bind() was used on the onClick event
   * to bind the event to only be triggered by the button.
   */
  function HandleDelete(id) {
    let sure = window.confirm("Are you sure you want to delete this event?");

    if (!sure) return;

    axios
      .delete(`${URL}/event/${id}`, { headers: { authorization: getToken() } })
      .then((res) => {
        alert("Success: Deleted!");
      })
      .catch((reason) => {
        handleHttpError(reason.message);
      });
  }

  return (
    <div className="left-align body">
      <h4>Conference Events</h4>
      <br />

      <table className="table table-dark table-striped">
        <thead>
          <tr>
            <td>Name</td>
            <td>Details</td>
            <td>Venue</td>
            <td>Date</td>
            <td>Time</td>
            <td>Entry Fee</td>
            <td>isActive</td> 
            <td style={{ display: isAdmin() ? "block" : "none" }}>Action</td>
          </tr>
        </thead>
        <tbody>
          {confEvents.map((evt) => {
            let date = new Date(evt.date);

            return (
              <tr key={evt._id}>
                <td>{evt.name}</td>
                <td>{evt.description}</td>
                <td>{evt.location}</td>
                <td>{date.toLocaleDateString()}</td>
                <td>{evt.duration}</td>
                <td>R{evt.entryPrice}</td>
                <td>{evt.isActive ? "Yes" : "No"}</td>
                <td style={{ display: isAdmin() ? "block" : "none" }}>
                  <input
                    type="button"
                    value="U"
                    title="Update/Edit"
                    className="btn btn-info"
                    onClick={HandleUpdate.bind(this, evt)}
                  />
                  {"\t\t"}
                  <input
                    type="button"
                    value="X"
                    title="Delete"
                    className="btn btn-danger"
                    onClick={HandleDelete.bind(this, evt._id)}
                  />
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

export default ListEvent;
