import React from "react";
import { withRouter } from "react-router-dom";
import { isAuthenticated, removeAuth } from "./sessionUtil";

/**
 * the history propery is created and automatically imported for us by the react-route-dom
 * when this component was declared  as a Route
 */

class NavBar extends React.Component {
  routeToRegister = () => {
    this.props.history.push("/register");
  };

  routeToListEvents = () => {
    this.props.history.push("/events/list");
  };

  routeToCreateEvent = () => {
    this.props.history.push("/events/create");
  };

  routeToLogin = () => {
    this.props.history.push("/login");
  };

  handleLogout = () => {
    let sure = window.confirm("Logout?");

    if (!sure) return;

    removeAuth();
    this.props.history.push("/login");
  };

  render() {
    return (
      <div
        className="row App left-align body"
        style={{
          backgroundColor: "#6ac57f",
        }}
      >
        <div
          className={`col-md-2 ${
            isAuthenticated() ? "hideSection" : "showSection"
          }`}
        >
          <div onClick={this.routeToRegister} className="link">
            Register
          </div>
        </div>
        <div
          className={`col-md-2 ${
            isAuthenticated() ? "showSection" : "hideSection"
          }`}
        >
          <div onClick={this.routeToCreateEvent} className="link">
            Create Event
          </div>
        </div>
        <div
          className={`col-md-2 ${
            isAuthenticated() ? "showSection" : "hideSection"
          }`}
        >
          <div onClick={this.routeToListEvents} className="link">
            View Events
          </div>
        </div>
        <div
          className="col-md-2"
          style={{ display: isAuthenticated() ? "none" : "block" }}
        >
          <div onClick={this.routeToLogin} className="link">
            Login
          </div>
        </div>
        <div
          className={`col-md-2 ${
            isAuthenticated() ? "showSection" : "hideSection"
          }`}
        >
          <div onClick={this.handleLogout} className="link">
            Logout
          </div>
        </div>
      </div>
    );
  }
}

export default withRouter(NavBar);
