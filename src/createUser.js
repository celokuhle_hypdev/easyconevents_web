import React from "react";
import User from "./models/user";
import { handleHttpError } from "./errorUtil";
import axios from "axios";
import URL from "./proxy";

class CreateUser extends React.Component {
  constructor(props) {
    super(props);
    let user = new User();
    user.isAdmin = false;
    this.state = {
      user,
    };
  }

  onSubmit = (event) => {
    event.preventDefault();

    axios
      .post(`${URL}/user`, this.state.user)
      .then((res) => {
        alert("Registration Successful.");
        this.props.history.push("/login");
      })
      .catch((reason) => {
        handleHttpError(reason.message);
      });
  };

  onStateChange = (event) => {
    let nam = event.target.name;
    let val = event.target.value;

    let temp = this.state.user;
    temp[nam] = val;

    this.setState({
      user: temp,
    });
  };

  onChecked = (event) => {
    let temp = this.state.user;
    temp.isAdmin = !temp.isAdmin;

    this.setState({
      isActive: temp,
    });
  };

  render() {
    return (
      <form className="left-align body" onSubmit={this.onSubmit}>
        <h4>Register new user account</h4>
        <br />
        <div className="row">
          <div className="col-md-3">
            <label>Name:</label>
          </div>
          <div className="col-md-4">
            <input
              name="name"
              className="full-width"
              onChange={this.onStateChange}
              required
            />
          </div>
        </div>
        <br />

        <div className="row">
          <div className="col-md-3">Surname:</div>
          <div className="col-md-4">
            <input
              name="surname"
              className="full-width"
              onChange={this.onStateChange}
              required
            />
          </div>
        </div>
        <br />

        <div className="row">
          <div className="col-md-3">Date Of Birth:</div>
          <div className="col-md-4">
            <input
              name="dateOfBirth"
              type="date"
              onChange={this.onStateChange}
              required
            />
          </div>
        </div>
        <br />

        <div className="row">
          <div className="col-md-3">Email:</div>
          <div className="col-md-4">
            <input
              type="email"
              name="email"
              className="full-width"
              onChange={this.onStateChange}
              required
            />
          </div>
        </div>
        <br />

        <div className="row">
          <div className="col-md-3">Password:</div>
          <div className="col-md-4">
            <input
              type="password"
              name="password"
              className="full-width"
              onChange={this.onStateChange}
              required
            />
          </div>
        </div>
        <br />

        <div className="row">
          <div className="col-md-3">Regiser As Admin</div>
          <div className="col-md-4">
            <input type="checkbox" name="isActive" onChange={this.onChecked} />
          </div>
        </div>
        <br />

        <div className="row">
          <div className="col-md-2">
            <br />
            <input type="submit" value="Submit" />
          </div>
        </div>
      </form>
    );
  }
}

export default CreateUser;
