import "./App.css";
import Header from "./header";
import ListEvent from "./listEvent";
import CreateEvent from "./createEvent";
import CreateUser from "./createUser";
import EditEvent from "./editEvent";
import NavBar from "./navBar";
import Login from "./login";
import { Switch, Route } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <Header />
      <NavBar />
      <br />
      <br />

      <Switch>
        <Route exact path="/" component={Login} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/register" component={CreateUser} />
        <Route exact path="/events/create" component={CreateEvent} />
        <Route exact path="/events/list" component={ListEvent} />
        <Route exact path="/events/edit/" component={EditEvent} />
      </Switch>
    </div>
  );
}

export default App;
