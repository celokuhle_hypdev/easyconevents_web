import jwt_decode from "jwt-decode";

export function setAuth(token) {
  sessionStorage.setItem("authToken", token);
}

export function removeAuth() {
  sessionStorage.removeItem("authToken");
}

export function isAuthenticated() {
  if (getToken() === null) {
    return false;
  }
  return true;
}

export function getToken() {
  return sessionStorage.getItem("authToken");
}
export function isAdmin() {
  let user = getDecodeJwtPayload();

  if (user !== null) {
    return user.isAdmin ? true : false;
  }
  return false;
}

function getDecodeJwtPayload() {
  try {
    return jwt_decode(getToken());
  } catch (error) {
    return null;
  }
}
