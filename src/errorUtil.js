import httpStatus from "./httpStatus";

export function handleHttpError(errMessage) {
  let msg = errMessage;

  if (errMessage.includes(httpStatus.UNAUTHORISED)) {
    msg = "Username or Password incorrect";
  } else if (errMessage.includes(httpStatus.ACCESS_DENIED)) {
    msg = "Access denied";
  } else if (errMessage.includes(httpStatus.BAD_REQUEST)) {
    msg = "Email/Username already be taken";
  } else if (errMessage.includes(httpStatus.INTERNAL_SERVER_ERROR)) {
    msg = "Intenal Server Error";
  } else {
    console.log("ERROR: " + errMessage);
  }
  alert(msg);
}
